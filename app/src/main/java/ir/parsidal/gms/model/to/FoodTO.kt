package ir.parsidal.gms.model.to

class FoodTO{
    var name = ""
    private var info = ""
    private var calorie: Float = 0f
    private var type = ""


    constructor(name: String, info: String, calorie: Float, type: String) {
        this.name = name
        this.info = info
        this.calorie = calorie
        this.type = type
    }



}