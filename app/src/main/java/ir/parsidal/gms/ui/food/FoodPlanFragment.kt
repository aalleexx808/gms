package ir.parsidal.gms.ui.food

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.parsidal.gms.R
import ir.parsidal.gms.component.VerticalButton
import ir.parsidal.gms.util.FoodRecyclerAdapter

class FoodPlanFragment : Fragment(), View.OnClickListener {

    private lateinit var foodPlanRecyclerView: RecyclerView
    private lateinit var foodRecyclerAdapter: FoodRecyclerAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var saturdayButton: VerticalButton
    private lateinit var sundayButton: VerticalButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root: View = inflater.inflate(R.layout.fragment_food_plan, container, false)

        createComponent(root)
        initAttribute(root)
        configFoodListRecyclerView(root)

        return root;
    }


    private fun createComponent(root: View) {
        saturdayButton = root.findViewById(R.id.food_plan_saturday_button)
        sundayButton = root.findViewById(R.id.food_plan_sunday_button)
    }

    private fun initAttribute(root: View) {
        saturdayButton.setOnClickListener(this)
        sundayButton.setOnClickListener(this)
    }

    private fun configFoodListRecyclerView(root: View) {
        viewManager = LinearLayoutManager(root.context)
        foodRecyclerAdapter = FoodRecyclerAdapter()
        foodPlanRecyclerView = root.findViewById(R.id.food_plan_recyclerview)
        foodPlanRecyclerView.adapter = foodRecyclerAdapter
        foodPlanRecyclerView.layoutManager = viewManager
        foodPlanRecyclerView.setHasFixedSize(true)
    }

    override fun onClick(view: View) {
        //todo: make select background
        if (view.id == R.id.food_plan_saturday_button) {
            saturdayButton.setBackgroundResource(R.drawable.bg_day_button_selected)
        } else if (view.id == R.id.food_plan_sunday_button) {
            sundayButton.setBackgroundResource(R.drawable.bg_day_button_selected)
        }
    }

}