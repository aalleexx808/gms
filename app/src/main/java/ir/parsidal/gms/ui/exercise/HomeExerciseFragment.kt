package ir.parsidal.gms.ui.exercise

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.parsidal.gms.R
import ir.parsidal.gms.component.HomeButton

class HomeExerciseFragment : Fragment(), View.OnClickListener {

    private lateinit var exerciseButton: HomeButton
    private lateinit var progressButton: HomeButton

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root: View = inflater.inflate(R.layout.fragment_exercise, container, false)
        createComponents(root)
        return root
    }

    private fun createComponents(root: View) {
        exerciseButton = root.findViewById(R.id.exercise_card_HomeButton)
        exerciseButton.setOnClickListener(this)
        progressButton = root.findViewById(R.id.progress_card_HomeButton)
        progressButton.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.exercise_card_HomeButton -> {
                val foodPlanFragment = WeekPlanFragment()
                val transaction = parentFragmentManager.beginTransaction()
                transaction.replace(R.id.nav_host_fragment, foodPlanFragment)
                transaction.commit()
            }
            R.id.progress_card_HomeButton -> {
                val progressFragment = ExerciseProgressFragment()
                val transaction = parentFragmentManager.beginTransaction()
                transaction.replace(R.id.nav_host_fragment, progressFragment)
                transaction.commit()
            }
        }
    }
}