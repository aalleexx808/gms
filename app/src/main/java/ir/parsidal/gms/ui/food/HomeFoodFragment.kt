package ir.parsidal.gms.ui.food

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.parsidal.gms.R
import ir.parsidal.gms.component.HomeButton

class HomeFoodFragment : Fragment(), View.OnClickListener {
    private var foodButton: HomeButton? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_home_food, container, false)
        createComponents(root)
        return root
    }

    private fun createComponents(root: View?) {
        foodButton = root?.findViewById(R.id.home_food_plan_card)
        foodButton?.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.home_food_plan_card -> {
                val foodPlanFragment = FoodPlanFragment()
                val transaction = parentFragmentManager.beginTransaction()
                transaction.replace(R.id.nav_host_fragment, foodPlanFragment)
                transaction.commit()
            }
        }
    }
}