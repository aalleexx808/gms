package ir.parsidal.gms.ui.exercise

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.parsidal.gms.R
import ir.parsidal.gms.util.DayOfWeekAdapter
import ir.parsidal.gms.util.ExerciseRecyclerAdapter

class WeekPlanFragment : Fragment(), View.OnClickListener {

    private lateinit var daysRecyclerView: RecyclerView
    private lateinit var dayOfWeekAdapter: DayOfWeekAdapter
    private lateinit var daysViewManager: RecyclerView.LayoutManager

    private lateinit var exerciseRecyclerView: RecyclerView
    private lateinit var exerciseRecyclerAdapter: ExerciseRecyclerAdapter
    private lateinit var exerciseViewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val root:View = inflater.inflate(R.layout.fragment_week_plan, container, false)

        createComponent(root)
        configDayListRecyclerView(root)
        configExerciseListRecyclerView(root)

        return root
    }

    private fun createComponent(root: View) {
    }

    private fun configDayListRecyclerView(root: View) {
        daysViewManager = LinearLayoutManager(root.context,LinearLayoutManager.HORIZONTAL,false)
        dayOfWeekAdapter = DayOfWeekAdapter()
        daysRecyclerView = root.findViewById(R.id.week_plan_days_recyclerview)
        daysRecyclerView.adapter = dayOfWeekAdapter
        daysRecyclerView.layoutManager = daysViewManager
        daysRecyclerView.setHasFixedSize(true)
    }

    private fun configExerciseListRecyclerView(root: View) {
        exerciseViewManager = LinearLayoutManager(root.context)
        exerciseRecyclerAdapter = ExerciseRecyclerAdapter()
        exerciseRecyclerView = root.findViewById(R.id.week_plan_exercise_recyclerview)
        exerciseRecyclerView.adapter = exerciseRecyclerAdapter
        exerciseRecyclerView.layoutManager = exerciseViewManager
        exerciseRecyclerView.setHasFixedSize(true)
    }

    override fun onClick(p0: View?) {
        TODO("Not yet implemented")
    }
}