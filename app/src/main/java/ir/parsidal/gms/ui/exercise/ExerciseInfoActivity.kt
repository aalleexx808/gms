package ir.parsidal.gms.ui.exercise

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import ir.parsidal.gms.R
import ir.parsidal.gms.util.ZoomOutPageTransformer

class ExerciseInfoActivity : FragmentActivity() {

    private lateinit var mPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise_info)

        mPager = findViewById(R.id.exercise_info_activity_viewpager)

        val pagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)

        mPager.adapter = pagerAdapter
        mPager.setPageTransformer(true, ZoomOutPageTransformer())
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = 5

        override fun getItem(position: Int): Fragment = ExerciseInfoFragment()
    }
}