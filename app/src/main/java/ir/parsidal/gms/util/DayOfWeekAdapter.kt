package ir.parsidal.gms.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.parsidal.gms.R

class DayOfWeekAdapter : RecyclerView.Adapter<DayOfWeekAdapter.Holder>() {

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dayButton: ImageButton = itemView.findViewById(R.id.day_of_week_circle_button)
        var dayText: TextView = itemView.findViewById(R.id.day_of_week_circle_textView)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayOfWeekAdapter.Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.day_of_week_circle, parent, false)

        return Holder(view)
    }

    override fun onBindViewHolder(holder: DayOfWeekAdapter.Holder, position: Int) {
        val viewHolder: DayOfWeekAdapter.Holder = holder
        //viewHolder.dayText.text = "شنبه ${position}"
        var days = holder.itemView.context.resources.getStringArray(R.array.DaysOfTheWeek)
        viewHolder.dayText.text = days[position]
    }

    override fun getItemCount(): Int = 7
}