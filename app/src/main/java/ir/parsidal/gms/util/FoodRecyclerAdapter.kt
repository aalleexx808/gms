package ir.parsidal.gms.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.parsidal.gms.R

class FoodRecyclerAdapter : RecyclerView.Adapter<FoodRecyclerAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.food_card_title_text)
        var content: TextView = itemView.findViewById(R.id.food_card_content_text)
        var type: TextView = itemView.findViewById(R.id.food_card_type_text)
        var icon: ImageView = itemView.findViewById(R.id.food_card_icon_image)
        var typeColor: ImageView = itemView.findViewById(R.id.food_card_color_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.food_card, parent, false)

        return MyViewHolder(view);
    }

    private val typeColorListForTest = intArrayOf(R.color.deepSaffron, R.color.danger, R.color.success)

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val viewHolder: MyViewHolder = holder
        viewHolder.title.text = "تغذیه شماره ${position + 1}"
        viewHolder.content.text = holder.itemView.context.getText(R.string.lorem)
        viewHolder.typeColor.setBackgroundResource(typeColorListForTest[position%3])

    }

    override fun getItemCount(): Int = 15
}