package ir.parsidal.gms.util

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import ir.parsidal.gms.R
import ir.parsidal.gms.ui.exercise.ExerciseInfoActivity

class ExerciseRecyclerAdapter: RecyclerView.Adapter<ExerciseRecyclerAdapter.Holder>() {
    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var card: CardView = itemView.findViewById(R.id.exercise_card_cardView)
        var title: TextView = itemView.findViewById(R.id.exercise_card_title_text)
        var type: TextView = itemView.findViewById(R.id.exercise_card_type_text)
        var icon: ImageView = itemView.findViewById(R.id.exercise_card_icon_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.exercise_card,parent,false)

        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val viewHolder: ExerciseRecyclerAdapter.Holder = holder
        viewHolder.card.setOnClickListener {
            val context: Context = holder.title.context
            //val intent = Intent(context, ExerciseInfoActivity::class.java)
            //context.startActivity(intent)

            val bottomSheet: BottomSheet = BottomSheet()
            val c = context as AppCompatActivity
            val manager: FragmentManager = c.supportFragmentManager
            bottomSheet.show(manager,BottomSheet.TAG)

        }


    }

    override fun getItemCount(): Int = 15
}