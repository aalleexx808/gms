package ir.parsidal.gms.util

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.parsidal.gms.R

class BottomSheet : BottomSheetDialogFragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.bottom_sheet_exercise_info, container, false)

        return view
    }

    companion object {
        const val TAG = "ModalBottomSheet"
    }
}