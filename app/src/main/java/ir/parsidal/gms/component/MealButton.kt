package ir.parsidal.gms.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import ir.parsidal.gms.R

class MealButton : LinearLayout {

    private var title: String? = "meal"
    private var _selected: Boolean = false

    constructor(context: Context?) : super(context) {

    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        View.inflate(context, R.layout.meal_button, this)
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.MealButton, defStyle, 0)

        title = a.getString(R.styleable.MealButton_text)

        _selected = a.getBoolean(R.styleable.MealButton_selected, _selected)

        a.recycle()

        initText()
    }

    private fun initText() {
        val textView: TextView = findViewById(R.id.meal_button_title)
        textView.text = title
    }

    fun getTitle(): String? = title

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getSelected(): Boolean = _selected

    fun setSelect(select: Boolean) {
        this._selected = select
    }

}