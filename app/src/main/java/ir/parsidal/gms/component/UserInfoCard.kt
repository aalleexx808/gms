package ir.parsidal.gms.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import ir.parsidal.gms.R

/**
 * TODO: document your custom view class.
 */
class UserInfoCard : LinearLayout {
    private var label: String? = null
    private var value: String? = null

    constructor(context: Context?) : super(context) {
        init(null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        View.inflate(context, R.layout.user_info_card, this)
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.UserInfoCard, defStyle, 0)
        label = a.getString(
                R.styleable.UserInfoCard_label)
        value = a.getString(
                R.styleable.UserInfoCard_value)
        val tvLabel = findViewById<TextView?>(R.id.user_info_card_label)
        val tvValue = findViewById<TextView?>(R.id.user_info_card_value)
        tvLabel.setText(label)
        tvValue.setText(value)
        a.recycle()
    }

    fun getLabel(): String? {
        return label
    }

    fun setLabel(label: String?) {
        this.label = label
    }

    fun getValue(): String? {
        return value
    }

    fun setValue(value: String?) {
        this.value = value
    }
}