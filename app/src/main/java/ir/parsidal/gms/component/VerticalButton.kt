package ir.parsidal.gms.component

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.text.TextPaint
import android.util.AttributeSet
import ir.parsidal.gms.R

class VerticalButton : androidx.appcompat.widget.AppCompatButton {

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec)
        setMeasuredDimension(measuredHeight, measuredWidth)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.VerticalButton, defStyle, 0)

        a.recycle()

    }



    override fun onDraw(canvas: Canvas) {
//        super.onDraw(canvas)
        val textPaint: TextPaint = paint
        textPaint.color = Color.BLACK
        textPaint.drawableState = drawableState
        val bounds: Rect = Rect()
        textPaint.getTextBounds("ENTER",0,"ENTER".length, bounds)

        canvas.save()
        //canvas.translate(0f, height * 1f);

        //canvas.translate(compoundPaddingLeft * 1f , extendedPaddingTop * 1f);
        //canvas.translate(width/2f - bounds.height(), ((height /2 + bounds.width()/1.5) * 1).toFloat());
        canvas.translate(width/2f - bounds.height(), height / 1f);
        canvas.rotate(-90f);

        layout.draw(canvas);
        canvas.restore();

    }
}
