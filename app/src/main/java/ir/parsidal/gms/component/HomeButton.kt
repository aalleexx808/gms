package ir.parsidal.gms.component

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.card.MaterialCardView
import ir.parsidal.gms.R

/**
 * TODO: document your custom view class.
 */
class HomeButton : MaterialCardView {
    private var title: String? = null
    private var startColor = Color.RED
    private var endColor = Color.RED
    private var titleColor = Color.WHITE
    private var icon: Drawable? = null

    constructor(context: Context?) : super(context) {
        init(null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {

        View.inflate(context, R.layout.home_button, this)
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.HomeButton, defStyle, 0)
        title = a.getString(
                R.styleable.HomeButton_title)
        startColor = a.getColor(
                R.styleable.HomeButton_startColor,
                startColor)
        endColor = a.getColor(
                R.styleable.HomeButton_endColor,
                endColor)
        titleColor = a.getColor(
                R.styleable.HomeButton_titleTextColor,
                titleColor)
        /*        icon_height = a.getInt(R.styleable.HomeButton_icon_height,icon_height);
        icon_width = a.getInt(R.styleable.HomeButton_icon_width,icon_width);*/
        if (a.hasValue(R.styleable.HomeButton_icon)) {
            icon = a.getDrawable(
                    R.styleable.HomeButton_icon)
            icon?.callback = this
        }
        a.recycle()
        initIcon()
        initTitle()
        radius = 4 * 8.toFloat()
        cardElevation = 15f
    }

    private fun initTitle() {
        val title: TextView = findViewById(R.id.home_button_title)
        title.text = this.title
        title.setTextColor(titleColor)
    }

    private fun initIcon() {
        val icon = findViewById<ImageView?>(R.id.home_button_icon)
        /*        icon.getLayoutParams().height = icon_height;
        icon.getLayoutParams().width = icon_width;*/
        icon?.background = this.icon
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val p = Paint()
        p.shader = RadialGradient(0f, height / 1f,
                width / 1f, startColor, endColor, Shader.TileMode.MIRROR)

        canvas!!.drawPaint(p)
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getStartColor(): Int {
        return startColor
    }

    fun setStartColor(startColor: Int) {
        this.startColor = startColor
    }

    fun getEndColor(): Int {
        return endColor
    }

    fun setEndColor(endColor: Int) {
        this.endColor = endColor
    }

    fun getTitleColor(): Int {
        return titleColor
    }

    fun setTitleColor(titleColor: Int) {
        this.titleColor = titleColor
    }

    fun getIcon(): Drawable? {
        return icon
    }

    fun setIcon(icon: Drawable?) {
        this.icon = icon
    }
}