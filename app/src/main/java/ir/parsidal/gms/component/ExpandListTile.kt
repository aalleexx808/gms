package ir.parsidal.gms.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import ir.parsidal.gms.R

class ExpandListTile(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    private var title: String?
    private var content: String?
    private lateinit var tileTextView: TextView
    private lateinit var contentTextView: TextView
    private lateinit var linearLayout: LinearLayout

    init {
        View.inflate(context, R.layout.custom_view_expand_list_tile, this)
        context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.ExpandListTile,
                0, 0).apply {
            try {
                title = getString(R.styleable.ExpandListTile_titleText)
                content = getString(R.styleable.ExpandListTile_contentText)

            } finally {
                recycle()
                createComponent()
                initText()
                initExpand()
            }
        }
    }

    private fun createComponent() {
        tileTextView = findViewById(R.id.expand_list_tile_title)
        contentTextView = findViewById(R.id.expand_list_tile_content)
         linearLayout = findViewById(R.id.expand_list_tile_parent_linearLayout)
    }

    private fun initExpand() {

        linearLayout.setOnClickListener {
            val params: ViewGroup.LayoutParams = contentTextView.layoutParams
            if (params.height == 0){
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                params.height = 0
            }
            contentTextView.layoutParams = params
        }
    }


    private fun initText() {
        tileTextView.text = title
        contentTextView.text = content
    }
}
