package ir.parsidal.gms

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView = findViewById<BottomNavigationView>(R.id.main_bottom_nav_view)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
/*        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_profile, R.id.navigation_food, R.id.navigation_exercise, R.id.navigation_extra2, R.id.navigation_extra1)
                .build();*/
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        //NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController)
        //navView.setOnNavigationItemSelectedListener(navListener);
    } /*
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()){
                        case R.id.navigation_exercise:
                            selectedFragment = new ExerciseFragment();
                            break;
                        case R.id.navigation_food:
                            selectedFragment = new FoodFragment();
                            break;
                        case R.id.navigation_profile:
                            selectedFragment = new ProfileFragment();
                            break;
                        case R.id.navigation_extra1:
                            selectedFragment = new Extra1Fragment();
                            break;
                        case R.id.navigation_extra2:
                            selectedFragment = new Extra2Fragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            android.R.anim.slide_in_left,  // enter
                            android.R.anim.fade_out,  // exit
                            android.R.anim.fade_in,   // popEnter
                            android.R.anim.slide_out_right  // popExit
                    ).replace(R.id.nav_host_fragment,selectedFragment).commit();

                    return true;
                }
            };
*/
}